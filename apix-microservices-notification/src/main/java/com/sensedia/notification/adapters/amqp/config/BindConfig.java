package com.sensedia.notification.adapters.amqp.config;

public class BindConfig {

  public static final String SUBSCRIBE_ACCOUNT_CREATED = "subscribeAccountCreated";
  public static final String PUBLISH_NOTIFICATION_OPERATION_ERROR = "publishNotificationOperationError";

  private BindConfig() {}
}
